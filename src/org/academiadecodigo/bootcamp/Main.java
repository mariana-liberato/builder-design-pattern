package org.academiadecodigo.bootcamp;
import org.academiadecodigo.bootcamp.character.*;
import org.academiadecodigo.bootcamp.character.Hero;


public class Main {

	public static void main(String[] args) {

		Hero.HeroBuilder heroBuilder = new Hero.HeroBuilder(Race.ELF, "Ruben");
		Hero hero = heroBuilder.buildArmor(Armour.FLAMING)
				.buildWeapon(Weapon.BOW)
				.buildHair(HairType.LONG)
				.build();
		System.out.println(hero);

	}
}
