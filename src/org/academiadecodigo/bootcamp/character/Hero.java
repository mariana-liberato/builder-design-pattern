package org.academiadecodigo.bootcamp.character;

public class Hero implements Character{

    private final Race race;
    private final String name;
    private final HairType hairType;
    private final EyesColor eyesColor;
    private final Armour armour;
    private final Weapon weapon;



    public Race getRace() {
        return race;
    }

    public String getName() {
        return name;
    }

    public HairType getHairType() {
        return hairType;
    }

    public EyesColor getEyesColor() {
        return eyesColor;
    }

    public Armour getArmour() {
        return armour;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {

        StringBuilder description = new StringBuilder();
        description.append("A new ");
        description.append(race);
        description.append(" named ");
        description.append(name);
        description.append(" appears. ");

        if (eyesColor != null) {
            description.append("It has ");
            description.append(eyesColor);
            description.append(" eyes. ");
        }

        if(hairType != null) {
            description.append("It's hair is ");
            description.append(hairType);
        }

        if (armour != null || weapon != null) {
            description.append("\n");
            description.append(name);
        }

        if (armour != null) {
            description.append(" is wearing the ");
            description.append(armour);
        }

        if (armour != null && weapon != null){
            description.append(" and");
        }

        if (weapon != null) {
            description.append(" is carrying a ");
            description.append(weapon);
        }

        description.append(".\n\n");

        return description.toString();
    }

    private Hero(HeroBuilder builder) {
        this.race = builder.race;
        this.name = builder.name;
        this.hairType = builder.hairType;
        this.eyesColor = builder.eyesColor;
        this.weapon = builder.weapon;
        this.armour = builder.armour;
    }

    public static class HeroBuilder implements CharacterBuilder{

        private final Race race;
        private final String name;
        private HairType hairType;
        private EyesColor eyesColor;
        private Armour armour;
        private Weapon weapon;

        public HeroBuilder(Race race, String name) {
            if (race == null || name == null) {
                throw new IllegalArgumentException("Race and name can't be null");
            }
            this.race = race;
            this.name = name;
        }

        public HeroBuilder buildHair(HairType hairType) {
            this.hairType = hairType;
            return this;
        }

        public HeroBuilder buildEyes(EyesColor eyesColor) {
            this.eyesColor = eyesColor;
            return this;
        }

        public HeroBuilder buildArmor(Armour armour) {
            this.armour = armour;
            return this;
        }

        public HeroBuilder buildWeapon(Weapon weapon) {
            this.weapon = weapon;
            return this;
        }

        /*//There are two options to build the object
        public Hero build(){
            return new Hero(race, name, hairType, eyesColor, armour, weapon);
        }*/


        public Hero build() {
            return new Hero(this);
        }
    }
}
