package org.academiadecodigo.bootcamp.character;

public interface CharacterBuilder {

    public CharacterBuilder buildHair(HairType hairType);

    public CharacterBuilder buildEyes(EyesColor eyesColor);

    public CharacterBuilder buildArmor(Armour armour);

    public CharacterBuilder buildWeapon(Weapon weapon);

    public Character build();

}
