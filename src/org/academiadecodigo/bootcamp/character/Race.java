package org.academiadecodigo.bootcamp.character;

public enum Race {

	ELF("elf"),
	WIZARD("wizard"),
	BORING_HUMAN("boring human"),
	HALFLING("halfling");

	private String raceName;

	Race(String raceName){
		this.raceName = raceName;
	}

	@Override
	public String toString() {
		return raceName;
	}

}
