package org.academiadecodigo.bootcamp.character;

public enum HairType {

    SHORT("short"),
    CURLY("curly"),
    LONG("long");

    private String hair;

    HairType(String hair) {
        this.hair = hair;
    }

    @Override
	public String toString() {
		return hair;
	}
}
