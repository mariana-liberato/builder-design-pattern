package org.academiadecodigo.bootcamp.character;

public enum EyesColor {

	WHITE("white"),
	PURPLE("purple"),
	RED("red"),
	BLUE("blue"),
	BLACK("black");

	private String color;

	EyesColor(String color){
		this.color = color;
	}



	@Override
	public String toString() {
		return color;
	}

}
