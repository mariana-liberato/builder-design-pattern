package org.academiadecodigo.bootcamp.character;

public enum Weapon {

	DAGGER("dagger"),
	WARHAMMER("warhammer"),
	AXE("axe"),
	SWORD("sword"),
    INTELLIGENCE("intelligence"),
	BOW("bow");

	private String weaponName;

	Weapon (String weaponName){
		this.weaponName = weaponName;
	}

	@Override
	public String toString() {
		return weaponName;
	}

}
