package org.academiadecodigo.bootcamp.character;

public enum Armour {

	INSULTS("Armour of Insults"),
    LEATHER("Leather Armour"),
    FLAMING("Flaming Armour");

    private String armourName;

    Armour(String armourName) {
        this.armourName = armourName;
    }

    @Override
	public String toString() {
		return armourName;
	}
}
